.. index::
   pair: Django ; autocomplete-light
   pair: Stencil ; autocomplete-light

.. _django_autocomplete_light_web_compo:

==========================================================================================================================================================
**autocomplete-light** : WebComponents for autocompletion with server-side HTML generation: the perfect match for server-side frameworks with **Stencil**
==========================================================================================================================================================

.. seealso::

   - https://yourlabs.io/oss/autocomplete-light
   - https://github.com/ionic-team/stencil





autocomplete-light
=======================

This package provides two **custom elements**:

- autocomplete-light: just an autocomplete input, you could use it for navigation
- autocomplete-select: maintain an HTML select, multiple is supported

Note that all components rely on server side rendering for the boxes, so that
you can theme them how you like without any extra configuration in
autocomplete-light.

**WebComponent by Stencil**
================================

.. seealso::

   - https://github.com/ionic-team/stencil
   - https://stenciljs.com/

This webcomponent uses StencilJS_.

Stencil is a compiler for building fast web apps using **Web Components**.

Stencil combines the best concepts of the most popular frontend frameworks
into a compile-time rather than run-time tool.

Stencil takes TypeScript, JSX, a tiny virtual DOM layer, efficient
one-way data binding, an asynchronous rendering pipeline (similar to React Fiber),
and lazy-loading out of the box, and generates 100% standards-based
Web Components that run in any browser supporting the Custom Elements v1 spec.

Stencil components are just Web Components, so they work in any major
framework or with no framework at all.


.. _StencilJS:  https://github.com/ionic-team/stencil
