.. index::
   pair: Django ; Components

.. _django_components:

==================================================================================
**Django Components** (Create simple reusable template components in Django)
==================================================================================

- https://github.com/EmilStenstrom/django-components/


Articles 2024
===============


2024-01-26 The Power of Django, HTMX and django-components
--------------------------------------------------------------

- https://www.pedaldrivenprogramming.com/2024/01/django-htmx-and-components/
