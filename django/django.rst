.. index::
   pair: Django ; Webcomponents

.. _django_web_components_ref:

=====================================
Django Web components
=====================================

.. toctree::
   :maxdepth: 3

   autocomplete_light/autocomplete_light
   django-components/django-components
   django-web-components/django-web-components
   djwc/djwc
   iommi/iommi
