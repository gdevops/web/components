.. index::
   pair: Web components; jsuites
   ! jsuites


.. _jsuites:

=====================================================================================================================
**jSuites** (**Javascript vanilla** web components)
=====================================================================================================================

.. seealso::

   - https://bossanova.uk/jsuites
   - https://github.com/paulhodel/jsuites
