
.. _jexcel_def:

=============================
**jExcel** definition
=============================




Definition
===========


**jExcel** CE is a **lightweight Vanilla JavaScript** plugin to create amazing
web-based interactive HTML tables and spreadsheets compatible with Excel
or any other spreadsheet software.

You can create an online spreadsheet table from a JS array, JSON, CSV or
XSLX files.

You can copy from excel and paste straight to your jExcel CE spreadsheet
and vice versa.

It is very easy to integrate any third party JavaScript plugins to create
your own custom columns, custom editors, and customize any feature into
your application.

jExcel CE has plenty of different input options through its native
column types to cover the most common web-based application requirements.

It is a complete solution for web data management.

Create amazing applications with jExcel CE JavaScript spreadsheet.

Main advantages
================

- Make rich and user-friendly web interfaces and applications.
- You can easily handle complicated data inputs in a way users are used..
- Improve your user software experience.
- Create rich CRUDS and beautiful UI.
- Compatibility with excel: users can move data around with common copy
  and paste shortcuts.
- Easy customizations with easy third-party plugin integrations.
- Lean, fast and simple to use.
- Thousands of successful user cases.
- Speed up your work dealing with difficult data entry in a web-based software.
