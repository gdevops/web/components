
.. _jexcel:

=====================================================================================================================
**jExcel** (a lightweight vanilla javascript plugin to create amazing web-based interactive tables and spreadsheets)
=====================================================================================================================

.. seealso::

   - https://github.com/paulhodel/jexcel
   - https://bossanova.uk/jexcel/v4/docs/getting-started
   - https://bossanova.uk/jexcel/v4/
   - https://x.com/paul_hodel

.. figure:: logo_jexcel.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
