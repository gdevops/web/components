
.. _javascript.info_web_components_tuto:

==========================================================================================
javascript.info web components tutorial
==========================================================================================

- https://javascript.info/web-components


.. figure:: web_components.png
   :align: center



Introduction
=============

Web components is a set of standards to make self-contained components:
custom HTML-elements with their own properties and methods, encapsulated
DOM and styles.
