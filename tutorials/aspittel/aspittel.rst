
.. _web_components_aspittel:

=================================================================================================
Building Web Components with Vanilla JavaScript by Ali Spittel (https://x.com/ASpittel)
=================================================================================================

.. seealso::

   - https://dev.to/aspittel/building-web-components-with-vanilla-javascript--jho
   - https://x.com/ASpittel
   - https://dev.to/aspittel




Learning Process
===================

I had a lot of difficulty finding articles and examples on web components
written in Vanilla JS.

There are a bunch of examples and articles on Polymer, which is a framework
for writing web components that includes polyfills for browsers that
don't support web components yet.

The framework sounds awesome, and I may try working with it in the future,
but I wanted to use just vanilla JavaScript for this particular project.

I ended up mostly using the MDN documentation on the Shadow DOM in order
to build my project.

I also looked through CodePen and the WebComponents site, though I didn't
find too much on either that was similar to what I wanted to build.

I also really liked Joseph Moore's article on web components which
came out while I was working on this project! It covered some of the
benefits of using web components:they work with all frameworks and
they are simple to implement and understand since they just use
vanilla JavaScript.
