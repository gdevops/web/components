
.. _notions_so_web_components_tuto:

=====================================
notion.so **Web components** tutorial
=====================================

.. seealso::

   - https://www.notion.so/Web-Components-bookmarks-64066078f891433dbc74997dc4d64302
   - https://www.notion.so/Introduction-648b4adbf5f349cd96e1903357a842ec





Components
============

.. seealso::

   - https://www.notion.so/a8b48550b8834cd086fc60919e9a0366?v=20b6428f6ded4e82bd9a3f337eafa047
