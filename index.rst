

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/web/components/rss.xml>`_


.. _web_components:

=====================
**Web components**
=====================

- https://en.wikipedia.org/wiki/Web_Components
- https://webcomponents.dev/new/
- https://webcomponents.dev/create/plainjs
- https://open-wc.org/
- https://www.notion.so/Web-Components-bookmarks-64066078f891433dbc74997dc4d64302
- https://dev.to/thatjoemoore/bringing-order-to-web-design-chaos--3fhb-
- https://developer.okta.com/blog/2019/12/18/vanilla-javascript-components
- https://developer.mozilla.org/en-US/docs/Web/API/Web_components


.. toctree::
   :maxdepth: 5

   articles/articles

.. toctree::
   :maxdepth: 3

   components/components
   django/django
   tutorials/tutorials
   javascript/javascript
